package com.Binarian;
import java.util.ArrayList;
import java.util.Scanner;


public class Main {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        boolean yOrN = true;

            System.out.println("\n||===========================================================||");
            System.out.println("||                Selamat datang pejuang !!!                 ||");
            System.out.println("||  Ayo daftarkan dirimu di guild dan jadilah petualang !!!  ||");
            System.out.println("S||===========================================================||\n");

        while (yOrN) {
            System.out.println("==========================");
            System.out.println("Pilihlah menu dibawah ini: ");
            System.out.println("==========================");
            System.out.println("1. Buat karakter");
            System.out.println("2. Lihat karakter");
            System.out.println("3. Hapus karakter");
            System.out.println("4. Keluar");
            System.out.print("Pilihan: ");
            String pilihan = input.nextLine();

            switch (pilihan) {
                case "1":
                    System.out.println("Buat karakter");
                    String n = inputNama();
                    Character character = new Character(n);
                    character.tambahNama(character);
                    character.cetakSeluruhNama();
                    break;

                case "2":
                    System.out.println("Lihat Karakter");

                    break;
                case "3":
                    System.out.println("Hapus karakter");
                    break;
                case "4":
                    System.out.println("keluar");
                    System.exit(0);
                    break;
                default:
                    System.err.println("Silahkan pilih menu yang tersedia");
                    yOrN = !pilihan.equalsIgnoreCase("y");
            }
            yOrN = getYOrN("Apakah kamu ingin melanjutkan?");
        }
    }

        private static boolean getYOrN(String pertanyaan){
            Scanner input = new Scanner(System.in);
            System.out.print("\n" + pertanyaan + " y/n ?: ");
            String pilihan = input.nextLine();

            while (!pilihan.equalsIgnoreCase("n") && !pilihan.equalsIgnoreCase("y")){
                System.err.println("Silahkan pilih y or n");
                System.out.print("\n" + pertanyaan + " y/n ?: ");
                pilihan = input.nextLine();
            }
            return pilihan.equalsIgnoreCase("y");
        }

        private static String inputNama(){
            Scanner input = new Scanner(System.in);
            System.out.print("Silahkan masukan nama anda: ");
            String nama = input.nextLine();
            return nama;
        }

        public void display (String character){
        System.out.println(character);
    }

        ArrayList<Character> daftarNama = new ArrayList<>();

        void tambahNama (Character c){
        daftarNama.add(c);
    }

        void cetakSeluruhNama (){
        System.out.println("Daftar seluruh Karakter: ");
        for (Character c : daftarNama){
            c.display();
        }
    }
}

